import React, {useState} from 'react';
import {Text, View} from 'react-native';
import {TextInput, Button} from 'react-native-paper';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const Search = (props) => {
  const [City, setCity] = useState('');
  // const [Cities, setCities] = useState([]);

  const fetchCities = (city) => {
    setCity(city);
    // fetch(`https://autocomplete.wunderground.com/aq?query=${city}`)
    //   .then((res) => res.json())
    //   .then((e) => setCities(e.RESULTS.slice(0, 9)))
    //   .catch((e) => console.log(e));
  };

  const btnClick = async () => {
    await AsyncStorage.setItem('city', City);
    props.navigation.navigate('Home', {city: City});
  };

  // const listClick = async (cityname) => {
  //  await AsyncStorage.setItem('city', City);
  //   setCity(cityname);
  //   props.navigation.navigate('Home', {city: cityname});
  // };

  return (
    <View style={{flex: 1}}>
      <TextInput
        value={City}
        label="city name"
        theme={{colors: {primary: '#00aaff'}}}
        onChangeText={(text) => fetchCities(text)}
      />
      <Button
        theme={{colors: {primary: '#00aaff'}}}
        icon="content-save"
        mode="contained"
        onPress={() => btnClick()}
        style={{margin: 20}}>
        <Text style={{color: '#FFF'}}>Search city</Text>
      </Button>

      {/* <FlatList
        data={Cities}
        keyExtractor={(e) => `${e.id}`}
        renderItem={({item}) => (
          <Card
            style={{margin: 2, padding: 2}}
            onPress={() => listClick(item.name)}>
            <Text>{item.name}</Text>
          </Card>
        )}
      /> */}
    </View>
  );
};
