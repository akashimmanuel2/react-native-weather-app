import React, {useEffect, useState} from 'react';
import {View, Image} from 'react-native';
import {Card, Title} from 'react-native-paper';
import {Header} from './Header';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const Home = (props) => {
  const [Info, setInfo] = useState({
    name: 'loading !!',
    temp: 'loading !!',
    humidity: 'loading !!',
    desc: 'loading !!',
  });

  useEffect(() => {
    getWeather();
  }, []);

  const getWeather = async () => {
    let Mycity = await AsyncStorage.getItem('city');
    if (!Mycity) {
      const {city} = props.route.params;
      Mycity = city;
    }

    fetch(
      `https://api.openweathermap.org/data/2.5/weather?q=${Mycity}&APPID=<APPID>&units=metric`,
    )
      .then((res) => res.json())
      .then((e) =>
        setInfo({
          name: e.name,
          temp: e.main.temp,
          humidity: e.main.humidity,
          desc: e.weather[0].description,
          icon: e.weather[0].icon,
        }),
      )
      .catch((e) => console.log(e));
  };

  if (props.route.params !== 'london') {
    getWeather();
  }

  return (
    <View style={{flex: 1}}>
      <Header title="Weather App" />
      <View style={{alignItems: 'center'}}>
        <Title style={{color: '#00aaff', marginTop: 30, fontSize: 20}}>
          {Info.name}
        </Title>
        <Image
          style={{height: 120, width: 120}}
          source={{uri: `https://openweathermap.org/img/w/${Info.icon}.png`}}
        />
      </View>
      <Card style={{margin: 5, padding: 12}}>
        <Title style={{color: '#00aaff'}}>Temperature - {Info.temp}</Title>
      </Card>
      <Card style={{margin: 5, padding: 12}}>
        <Title style={{color: '#00aaff'}}>Humidity - {Info.humidity}</Title>
      </Card>
      <Card style={{margin: 5, padding: 12}}>
        <Title style={{color: '#00aaff'}}>Description - {Info.desc}</Title>
      </Card>
    </View>
  );
};
