import * as React from 'react';
import {Appbar, Title} from 'react-native-paper';

export const Header = (props) => {
  return (
    <Appbar.Header
      theme={{
        colors: {
          primary: '#00aaff',
        },
      }}
      style={{flexDirection: 'row', justifyContent: 'center'}}>
      <Title style={{color: '#FFF'}}>{props.title}</Title>
    </Appbar.Header>
  );
};
